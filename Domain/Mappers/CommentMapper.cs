﻿using AutoMapper;
using Domain.Models;

namespace Domain.Mappers
{
    public class CommentMapper : Profile
    {
        public CommentMapper()
        {
            CreateMap<CommentRequest, CommentResponse>()
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => DateTimeOffset.FromUnixTimeSeconds(src.CreatedAt).UtcDateTime));
        }
    }
}
