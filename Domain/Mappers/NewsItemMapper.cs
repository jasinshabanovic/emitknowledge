﻿using AutoMapper;
using Domain.Models;

namespace Domain.Mappers
{
    public class NewsItemMapper : Profile
    {
        public NewsItemMapper()
        {
            CreateMap<NewsItemRequest, NewsItemResponse>()
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => DateTimeOffset.FromUnixTimeSeconds(src.CreatedAt).UtcDateTime));
        }
    }
}
