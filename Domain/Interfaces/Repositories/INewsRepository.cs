﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface INewsRepository
    {
        Task<IEnumerable<NewsItemRequest>> GetNewsAsync();
        Task<NewsItemRequest> GetNewsItemByIdAsync(int id);
        Task<IEnumerable<CommentRequest>> GetCommentsForNewsItemAsync(List<int> commentIds);
    }
}
