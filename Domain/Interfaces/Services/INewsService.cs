﻿using Domain.Models;
namespace Domain.Interfaces.Services
{
    public interface INewsService
    {
        Task<IEnumerable<NewsItemResponse>> GetNewsAsync(string? filter, string? keyword);
        Task<NewsItemResponse> GetNewsItemByIdAsync(int id);
        /*Task<IEnumerable<CommentRequest>> GetCommentsForNewsItemAsync(int newsItemId);*/
    }
}
