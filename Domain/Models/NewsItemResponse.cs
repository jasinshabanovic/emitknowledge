﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class NewsItemResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int Vote { get; set; }
        public int CommentCounts { get; set; }
        public string Type { get; set; }
        public string Author { get; set; }
        public List<CommentResponse> Comments { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
