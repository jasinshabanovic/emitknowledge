﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Domain.Models
{
    public class CommentResponse
    {
        public string Author { get; set; }
        public int CommentId { get; set; }
        public int ParentCommentId { get; set; }
        public string CommentText { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CommentType { get; set; }
        public List<CommentResponse> ChildComments { get; set; }
    }

}
