﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Domain.Models
{
    public class CommentRequest
    {
        [JsonProperty("by")]
        public string Author { get; set; }
        [JsonProperty("id")]
        public int CommentId { get; set; }
        [JsonProperty("kids")]
        public List<int> ChildCommentIds { get; set; }
        [JsonProperty("parent")]
        public int ParentCommentId { get; set; }
        [JsonProperty("text")]
        public string CommentText { get; set; }
        [JsonProperty("time")]
        public int CreatedAt { get; set; }
        [JsonProperty("type")]
        public string CommentType { get; set; }
        public List<CommentRequest> ChildComments { get; set; }
    }

}
