﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class NewsItemRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("score")]
        public int Vote { get; set; }
        [JsonProperty("descendants")]
        public int CommentCounts { get; set; } 
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("by")]
        public string Author { get; set; }
        [JsonProperty("kids")]
        public List<int> CommentIds { get; set; }
        [JsonProperty("time")]
        public int CreatedAt { get; set; }
    }
}
