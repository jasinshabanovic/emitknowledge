using Azure;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Claims.Controllers
{
    [Route("api/news")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetNews([FromQuery] string? filter, [FromQuery] string? keyword)
        {
            var news = await _newsService.GetNewsAsync(filter, keyword);
            return Ok(news);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetNewsItem(int id)
        {
            var newsItem = await _newsService.GetNewsItemByIdAsync(id);
            if (newsItem == null)
                return NotFound();

            return Ok(newsItem);
        }
    }

}
