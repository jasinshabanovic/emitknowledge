﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Newtonsoft.Json;

namespace Repositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly HttpClient _httpClient;

        public NewsRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<NewsItemRequest>> GetNewsAsync()
        {
            var response = await _httpClient.GetAsync("https://hacker-news.firebaseio.com/v0/newstories.json");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed to fetch news from Hacker News API. Status code: {response.StatusCode}");
            }

            var storyIds = JsonConvert.DeserializeObject<IEnumerable<int>>(await response.Content.ReadAsStringAsync());
            var newsItems = new List<NewsItemRequest>();

            foreach (var storyId in storyIds.Take(10))
            {
                var story = await GetStoryByIdAsync(storyId);
                if (story != null)
                {
                    newsItems.Add(story);
                }
            }

            return newsItems;
        }

        public async Task<NewsItemRequest> GetStoryByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{id}.json");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed to fetch news item with ID {id} from Hacker News API. Status code: {response.StatusCode}");
            }

            var json = await response.Content.ReadAsStringAsync();
            var newsItem = JsonConvert.DeserializeObject<NewsItemRequest>(json);

            return newsItem;
        }

        public async Task<NewsItemRequest> GetNewsItemByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{id}.json");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<NewsItemRequest>(json);
        }

        public async Task<IEnumerable<CommentRequest>> GetCommentsForNewsItemAsync(int newsItemId)
        {
            var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{newsItemId}.json");
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Failed to fetch comments for news item from Hacker News API. Status code: {response.StatusCode}");
            }

            var json = await response.Content.ReadAsStringAsync();
            var comments = JsonConvert.DeserializeObject<CommentRequest>(json);

            return new List<CommentRequest>();
        }

        /*public async Task<IEnumerable<CommentRequest>> GetCommentsForNewsItemAsync(List<int> commentIds)
        {
            var comments = new List<CommentRequest>();

            foreach (var commentId in commentIds)
            {
                var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{commentId}.json");
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Failed to fetch comment {commentId} from Hacker News API. Status code: {response.StatusCode}");
                }

                var json = await response.Content.ReadAsStringAsync();
                var comment = JsonConvert.DeserializeObject<CommentRequest>(json);

                if (comment != null)
                {
                    comments.Add(comment);
                }
            }

            return comments;
        }*/

        public async Task<IEnumerable<CommentRequest>> GetCommentsForNewsItemAsync(List<int> commentIds)
        {
            var comments = new List<CommentRequest>();
            if (commentIds != null)
            {
                foreach (var commentId in commentIds)
                {
                    var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{commentId}.json");
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception($"Failed to fetch comment {commentId} from Hacker News API. Status code: {response.StatusCode}");
                    }

                    var json = await response.Content.ReadAsStringAsync();
                    var comment = JsonConvert.DeserializeObject<CommentRequest>(json);

                    if (comment != null)
                    {
                        comment.ChildComments = await GetChildCommentsAsync(comment.ChildCommentIds);
                        comments.Add(comment);
                    }
                }
            }

            return comments;
        }

        private async Task<List<CommentRequest>> GetChildCommentsAsync(List<int> childCommentIds)
        {
            var childComments = new List<CommentRequest>();

            if (childCommentIds != null)
            {
                foreach (var childCommentId in childCommentIds)
                {
                    var response = await _httpClient.GetAsync($"https://hacker-news.firebaseio.com/v0/item/{childCommentId}.json");
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception($"Failed to fetch comment {childCommentId} from Hacker News API. Status code: {response.StatusCode}");
                    }

                    var json = await response.Content.ReadAsStringAsync();
                    var childComment = JsonConvert.DeserializeObject<CommentRequest>(json);

                    if (childComment != null)
                    {
                        childComment.ChildComments = await GetChildCommentsAsync(childComment.ChildCommentIds);
                        childComments.Add(childComment);
                    }
                }
            }

            return childComments;
        }


    }

}
