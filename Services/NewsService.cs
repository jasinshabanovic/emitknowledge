﻿using AutoMapper;
using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.Azure.Cosmos.Core.Collections;

namespace Services
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _repository;
        private readonly IMapper _mapper;

        public NewsService(INewsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<NewsItemResponse>> GetNewsAsync(string? filter, string? keyword)
        {
            IEnumerable<NewsItemRequest> news = await _repository.GetNewsAsync();

            if (!string.IsNullOrWhiteSpace(filter))
            {
                switch (filter.ToLower())
                {
                    case "newest":
                    case "all":
                        break;
                    case "hot":
                        news = news.OrderByDescending(n => n.Vote);
                        break;
                    case "showhn":
                        news = news.Where(n => n.Type == "story" && n.Title.StartsWith("Show HN:"));
                        break;
                    case "askhn":
                        news = news.Where(n => n.Type == "story" && n.Title.StartsWith("Ask HN:"));
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrWhiteSpace(keyword))
            {
                keyword = keyword.ToLower();
                news = news.Where(n => n.Title.ToLower().Contains(keyword));
            }

            return _mapper.Map<List<NewsItemResponse>>(news.ToList()); 
        }

        public async Task<NewsItemResponse> GetNewsItemByIdAsync(int id)
        {
            var response = await _repository.GetNewsItemByIdAsync(id);
            var comments = await _repository.GetCommentsForNewsItemAsync(response.CommentIds);
            var mappedNewsItem = _mapper.Map<NewsItemResponse>(response);
            mappedNewsItem.Comments = _mapper.Map<List<CommentResponse>>(comments.ToList());

            return mappedNewsItem;
        }

       /* public async Task<IEnumerable<Comment>> GetCommentsForNewsItemAsync(int newsItemId)
        {
            return await _repository.GetCommentsForNewsItemAsync(newsItemId);
        }*/
    }
}
